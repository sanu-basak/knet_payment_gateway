<?php

namespace App\Services\Knet;

use App\Services\Knet\KPayClient;
use GuzzleHttp\Client;



class KPayManager extends KPayClient
{
    protected $id = null;
    protected $password = null;
    protected $action = null;
    protected $langid = null;
    protected $currencycode = null;
    protected $responseURL = null;
    protected $errorURL = null;
    protected $amt = null;
    protected $trackid = null;
    protected $udf1 = 0; // Order Id
    protected $udf2 = 'Test2';
    protected $udf3 = 'Test3';
    protected $udf4 = 'Test4';
    protected $udf5 = 'Test5';
    protected $user_id = null;
    protected $result = null;
    protected $transactionAmt = 0;
    protected $resourceKey=null;

    // url params
    protected $trandata = null;
    protected $tranportalId = null;

    private $paramsToEncrypt = ['id', 'password', 'action', 'langid', 'currencycode', 'amt', 'responseURL', 'errorURL', 'trackid', 'udf1', 'udf2', 'udf3', 'udf4', 'udf5'];
    protected $reqParams = ['trandata', 'tranportalId', 'responseURL', 'errorURL'];
    private $params = null;

    public function __construct()
    {
        $this->checkForResourceKey();
        $this->initiatePaymentConfig();
    }

    private function initiatePaymentConfig()
    { 
        $this->id = config('knet.transport.id');
        $this->tranportalId = config('knet.transport.id');
        $this->password = config('knet.transport.password');

        $this->resourceKey = config('knet.resource_key');

        $this->action = config('knet.action_code', 1);
        $this->langid = config('knet.language', 'EN');
        $this->currencycode = config('knet.currency', 414);

        $this->responseURL = url(config('knet.response_url'));
        $this->errorURL = url(config('knet.error_url'));

        $this->udf1 = request()->orderId;

        $this->params = 'id='.$this->tranportalId."&password=".$this->password."&action=".$this->action."&langid=".$this->langid."&currencycode=".$this->currencycode."&amt=".request()->transactionAmount."&responseURL=".$this->responseURL."&errorURL=".$this->errorURL."&trackid=".mt_rand()."&udf1=".$this->udf1."&udf2=".$this->udf2."&udf3=".$this->udf3."&udf4=".$this->udf4."&udf5=".$this->udf5;
        
        $this->params = $this->encryptAES($this->params,$this->resourceKey)."&tranportalId=".$this->tranportalId."&responseURL=".$this->responseURL."&errorURL=".$this->errorURL;
    }

    /**
     * check for existence of resource key
     *
     * @throws KnetException
     */
    private function checkForResourceKey()
    {
        if (config('knet.resource_key') == null) {
            return response()->json(['status'=>'error','message'=>'Missing resource key'],422);
        }
    }

    public function actionUrl()
    {
        return $this->getEnvUrl().'&trandata='.$this->params;
    }

    private function getEnvUrl()
    {
        $url = config('knet.development_url');

        if (\App::environment(['production'])) {
            if (!env('KNET_DEBUG')) {
                $url = config('knet.production_url');
            }
        }

        return $url . '?param=paymentInit';
    }



}

