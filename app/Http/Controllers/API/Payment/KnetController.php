<?php

namespace App\Http\Controllers\API\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Knet\KPayManager;
use Validator;
use Api;

class KnetController extends Controller
{
    public function payment(Request $request)
    {
        try {

            $validator = Validator::make($request->all(),[
                'transactionAmount'    => 'required',
                'orderId'              => 'required'
            ]);
    
            if($validator->fails()){
    
                return response()->json(Api::validationResponse($validator),422); 
            }
    

            $knet = new KPayManager();

            return response()->json([
                'status'  => 'success',
                'message' => 'Payment gateway link',
                'data'    => [
                    'paymentLink' => $knet->actionUrl()
                ]
            ]);

        }catch(\Exception $e) {

            return response()->json([
                'status'  => 'error',
                'message' => 'Payment gateway not responding'
            ],500);
        }
       
        
    }

  

    public function success(Request $request)
    {

        $knet = new KPayManager();

        $responses = $knet->decryptAES($request->trandata, config('knet.resource_key'));

        // \Log::info($request->all());

        if(!empty($request->ErrorText)){

            // $params = 'paymentid='.$request->paymentid."&trackid=".$request->trackid."&ErrorText=".$request->ErrorText."&amount=".$request->amt."&udf1=".$request->udf1;

    
            $url = 'http://www.batzawaj.com/payment-failure?'.$responses;
    
        }else{

            // $params = 'paymentid='.$request->paymentid."&trackid=".$request->trackid."&ErrorText=".$request->ErrorText."&amount=".$request->amt."&udf1=".$request->udf1;

    
            $url = 'http://www.batzawaj.com/payment-success?'.$responses;
        }


       
        return redirect()->away($url);


        return view('welcome',compact('request'));
    }


}
