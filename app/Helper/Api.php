<?php 

namespace App\Helper;

class Api 
{ 
    public static function apiSuccessResponse($message,$data=NULL,$key = [],$status=false)
    {
        $success = [
            'status'       => 'success',
            'message'       => $message,  
            'data'          => $data
        ];

        return $success;
    }

    public static function validationResponse($validator)
    {
        foreach($validator->errors()->toArray() as $v => $a){

            $validationError = [
                'status'       => 'error',
                'message'      => $a[0],
            ];
    
            return $validationError;
            
        }

    }

    public static function apiErrorResponse($message)
    {
        $error = [
            'status'       => 'error',
            'message'       => $message,
        ];

        return $error;
    }

}